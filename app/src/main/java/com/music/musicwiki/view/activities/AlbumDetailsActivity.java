package com.music.musicwiki.view.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.ag.lfm.LfmError;
import com.ag.lfm.LfmParameters;
import com.ag.lfm.LfmRequest;
import com.ag.lfm.api.methods.LfmApiAlbum;
import com.ag.lfm.api.methods.LfmApiTag;
import com.music.musicwiki.R;
import com.music.musicwiki.view.adapters.GenreRecyclerAdapter;
import com.music.musicwiki.model.Genres;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AlbumDetailsActivity extends AppCompatActivity {

    private CardView backButtonCardView;
    private ImageView albumCoverIV;
    private List<Genres> genresList;
    private LfmApiAlbum lfmApiAlbum;
    private LfmApiTag lfmApiTag;
    private LfmParameters lfmParameters;
    private RecyclerView genreRecyclerView2;
    private TextView albumTitleTextView, artistNameTextView, descTextView;

    private void getInfo() {

        lfmApiAlbum.getInfo(lfmParameters).executeWithListener(new LfmRequest.LfmRequestListener() {
            @Override
            public void onComplete(JSONObject response) {

                JSONObject info = response.optJSONObject("album");

                if (info != null) {

                    String name = info.optString("name");
                    String artist = info.optString("artist");
                    albumCoverIV.setImageResource(R.drawable.music);
                    albumTitleTextView.setText(name);
                    artistNameTextView.setText(artist);

                }

            }

            @Override
            public void onError(LfmError error) {

                Log.e("error", error.errorMessage);
            }
        });
    }

    private void getTags() {

        LfmParameters params = new LfmParameters();
        lfmApiTag.getTopTags(params).executeWithListener(new LfmRequest.LfmRequestListener() {
            @Override
            public void onComplete(JSONObject response) {

                genresList.clear();
                JSONArray topTags = Objects.requireNonNull(response.optJSONObject("toptags")).optJSONArray("tag");

                for (int i = 0; i < Objects.requireNonNull(topTags).length(); i++) {

                    genresList.add(new Genres(topTags.optJSONObject(i).optString("name")));

                }

                GenreRecyclerAdapter genreRecyclerAdapter = new GenreRecyclerAdapter(genresList);
                genreRecyclerView2.setAdapter(genreRecyclerAdapter);
                genreRecyclerAdapter.setOnGenreClickListener(genreTitle -> {

                    Intent intent = new Intent(AlbumDetailsActivity.this, GenreDetailsActivity.class);
                    intent.putExtra(MainActivity.GENRE_TITLE, genreTitle);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                });
            }

            @Override
            public void onError(LfmError error) {

                Log.e("error", error.errorMessage);
            }
        });
    }

    private void initializeViews() {

        albumCoverIV = findViewById(R.id.albumCoverIV);
        albumTitleTextView = findViewById(R.id.albumTitleTextView);
        artistNameTextView = findViewById(R.id.artistNameTextView);
        backButtonCardView = findViewById(R.id.backButtonCardView);
        descTextView = findViewById(R.id.descTextView);
        genresList = new ArrayList<>();
        genreRecyclerView2 = findViewById(R.id.genreRecyclerView2);
        genreRecyclerView2.setHasFixedSize(true);
        genreRecyclerView2.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        lfmApiAlbum = new LfmApiAlbum();
        lfmApiTag = new LfmApiTag();
        lfmParameters = new LfmParameters();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_details);

        initializeViews();
        backButtonCardView.setOnClickListener(v -> finish());
        Intent intent = getIntent();
        String album_title = intent.getStringExtra("album_title");
        String artist_name = intent.getStringExtra("artist_name");
        lfmParameters.put("artist", artist_name);
        lfmParameters.put("album", album_title);
        lfmParameters.put("api_key", getResources().getString(R.string.api_key));
        getInfo();
        getTags();
    }
}