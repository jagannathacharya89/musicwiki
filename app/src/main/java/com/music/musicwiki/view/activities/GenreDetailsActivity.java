package com.music.musicwiki.view.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.ag.lfm.LfmError;
import com.ag.lfm.LfmParameters;
import com.ag.lfm.LfmRequest;
import com.ag.lfm.api.methods.LfmApiTag;
import com.google.android.material.tabs.TabLayout;
import com.music.musicwiki.R;
import com.music.musicwiki.view.adapters.GenreViewPagerAdapter;
import com.music.musicwiki.view.fragments.AlbumsFragment;
import com.music.musicwiki.view.fragments.ArtistsFragment;
import com.music.musicwiki.view.fragments.TracksFragment;

import org.json.JSONException;
import org.json.JSONObject;

public class GenreDetailsActivity extends AppCompatActivity implements AlbumsFragment.AlbumListener, ArtistsFragment.ArtistListener {

    private CardView backButtonCardView;
    private LfmApiTag lfmApiTag;
    private LfmParameters params;
    private TextView genreDescriptionTextView;

    private void getGenreDescription() {

        lfmApiTag.getInfo(params).executeWithListener(new LfmRequest.LfmRequestListener() {
            @Override
            public void onComplete(JSONObject response) {

                try {

                    JSONObject tag = response.getJSONObject("tag");
                    String description = tag.getJSONObject("wiki").getString("summary");
                    char[] des_array = description.toCharArray();
                    StringBuilder builder = new StringBuilder();

                    for (char c : des_array) {

                        if (c != '<') {

                            builder.append(c);


                        } else {

                            break;

                        }

                    }

                    String newDesc = builder.toString();
                    genreDescriptionTextView.setText(newDesc);

                } catch (JSONException e) {

                    e.printStackTrace();

                }

            }

            @Override
            public void onError(LfmError error) {

                Log.e("error", error.errorMessage);
            }
        });
    }

    private void initializeViews() {

        backButtonCardView = findViewById(R.id.backButtonCardView);
        genreDescriptionTextView = findViewById(R.id.genreDescriptionTextView);
        TextView genreTitleTextView = findViewById(R.id.genreTitleTextView);
        GenreViewPagerAdapter genreViewPagerAdapter = new GenreViewPagerAdapter(getSupportFragmentManager(), 1);
        TabLayout genreTabLayout = findViewById(R.id.genreTabLayout);
        ViewPager genreViewPager = findViewById(R.id.genreViewPager);
        Intent intent = getIntent();
        String genreTitle = intent.getStringExtra(MainActivity.GENRE_TITLE);
        genreTitleTextView.setText(genreTitle);
        lfmApiTag = new LfmApiTag();
        params = new LfmParameters();
        genreViewPagerAdapter.addFragment(new AlbumsFragment(getResources().getString(R.string.api_key), genreTitle), "ALBUMS");
        genreViewPagerAdapter.addFragment(new ArtistsFragment(getResources().getString(R.string.api_key), genreTitle), "ARTISTS");
        genreViewPagerAdapter.addFragment(new TracksFragment(getResources().getString(R.string.api_key), genreTitle), "TRACKS");
        genreViewPager.setAdapter(genreViewPagerAdapter);
        genreTabLayout.setupWithViewPager(genreViewPager);
        params.put("tag", genreTitle);
        params.put("api_key", getResources().getString(R.string.api_key));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genre_details);

        initializeViews();
        backButtonCardView.setOnClickListener(v -> finish());
        getGenreDescription();
    }

    @Override
    public void onAlbumClick(String album_title, String artist_name) {

        Intent intent = new Intent(this, AlbumDetailsActivity.class);
        intent.putExtra("album_title", album_title);
        intent.putExtra("artist_name", artist_name);
        startActivity(intent);
    }

    @Override
    public void onArtistClick(String artist_name) {

        Intent intent = new Intent(this, ArtistDetailsActivity.class);
        intent.putExtra("artist_name", artist_name);
        startActivity(intent);
    }
}