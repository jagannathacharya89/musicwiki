package com.music.musicwiki.view.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.ag.lfm.LfmError;
import com.ag.lfm.LfmParameters;
import com.ag.lfm.LfmRequest;
import com.ag.lfm.api.methods.LfmApiAlbum;
import com.ag.lfm.api.methods.LfmApiTag;
import com.music.musicwiki.R;
import com.music.musicwiki.view.adapters.GenreRecyclerAdapter;
import com.music.musicwiki.model.Genres;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ArtistDetailsActivity extends AppCompatActivity {

    private CardView backButtonCardView;
    private ImageView albumCoverIV;
    private List<Genres> genresList;
    private LfmApiAlbum lfmApiAlbum;
    private LfmApiTag lfmApiTag;
    private LfmParameters lfmParameters;
    private RecyclerView genreRecyclerView3;
    private TextView artistNameTextView, followersTextView, playCountTextView;

    private void getTags() {

        LfmParameters params = new LfmParameters();
        lfmApiTag.getTopTags(params).executeWithListener(new LfmRequest.LfmRequestListener() {
            @Override
            public void onComplete(JSONObject response) {

                genresList.clear();
                JSONArray topTags = Objects.requireNonNull(response.optJSONObject("toptags")).optJSONArray("tag");

                for (int i = 0; i < Objects.requireNonNull(topTags).length(); i++) {

                    genresList.add(new Genres(topTags.optJSONObject(i).optString("name")));

                }

                GenreRecyclerAdapter genreRecyclerAdapter = new GenreRecyclerAdapter(genresList);
                genreRecyclerView3.setAdapter(genreRecyclerAdapter);
                genreRecyclerAdapter.setOnGenreClickListener(genreTitle -> {

                    Intent intent = new Intent(ArtistDetailsActivity.this, GenreDetailsActivity.class);
                    intent.putExtra(MainActivity.GENRE_TITLE, genreTitle);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                });
            }

            @Override
            public void onError(LfmError error) {

                Log.e("error", error.errorMessage);
            }
        });
    }

    private void initializeViews() {

        albumCoverIV = findViewById(R.id.albumCoverIV);
        albumCoverIV.setImageResource(R.drawable.music);
        artistNameTextView = findViewById(R.id.artistNameTextView);
        backButtonCardView = findViewById(R.id.backButtonCardView);
        followersTextView = findViewById(R.id.followersTextView);
        playCountTextView = findViewById(R.id.playCountTextView);
        genresList = new ArrayList<>();
        genreRecyclerView3 = findViewById(R.id.genreRecyclerView3);
        genreRecyclerView3.setHasFixedSize(true);
        genreRecyclerView3.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        lfmApiAlbum = new LfmApiAlbum();
        lfmApiTag = new LfmApiTag();
        lfmParameters = new LfmParameters();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_details);

        initializeViews();
        backButtonCardView.setOnClickListener(v -> finish());
        Intent intent = getIntent();
        String artist_name = intent.getStringExtra("artist_name");
        lfmParameters.put("artist", artist_name);
        lfmParameters.put("api_key", getResources().getString(R.string.api_key));
        albumCoverIV.setImageResource(R.drawable.music);
        artistNameTextView.setText(artist_name);
        getTags();
    }
}