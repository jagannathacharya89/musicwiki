package com.music.musicwiki.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ag.lfm.LfmError;
import com.ag.lfm.LfmParameters;
import com.ag.lfm.LfmRequest;
import com.ag.lfm.api.methods.LfmApiTag;
import com.music.musicwiki.R;
import com.music.musicwiki.view.adapters.TopAlbumRecyclerAdapter;
import com.music.musicwiki.model.TopAlbums;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AlbumsFragment extends Fragment {

    private AlbumListener albumListener;
    private final String api_key, tag;
    private LfmApiTag lfmApiTag;
    private LfmParameters params;
    private List<TopAlbums> topAlbumsList;
    private RecyclerView topAlbumsRecyclerView;

    public AlbumsFragment(String api_key, String tag) {

        this.api_key = api_key;
        this.tag = tag;
    }

    private void initializeViews(View view) {

        lfmApiTag = new LfmApiTag();
        params = new LfmParameters();
        topAlbumsList = new ArrayList<>();
        topAlbumsRecyclerView = view.findViewById(R.id.topAlbumsRecyclerView);
        topAlbumsRecyclerView.setHasFixedSize(true);
        topAlbumsRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
    }

    private void getTopAlbums() {

        params.put("tag", tag);
        params.put("api_key", api_key);
        lfmApiTag.getTopAlbums(params).executeWithListener(new LfmRequest.LfmRequestListener() {
            @Override
            public void onComplete(JSONObject response) {

                topAlbumsList.clear();
                JSONArray topAlbums = Objects.requireNonNull(response.optJSONObject("albums")).optJSONArray("album");

                for (int i = 0; i < Objects.requireNonNull(topAlbums).length(); i++) {

                    String album_title = topAlbums.optJSONObject(i).optString("name");
                    String album_cover = topAlbums.optJSONObject(i).optString("url");
                    Log.d("cover", album_cover);
                    String artist_name = Objects.requireNonNull(topAlbums.optJSONObject(i).optJSONObject("artist")).optString("name");
                    topAlbumsList.add(new TopAlbums(
                            album_cover,
                            album_title,
                            artist_name)
                    );

                }

                TopAlbumRecyclerAdapter topAlbumRecyclerAdapter = new TopAlbumRecyclerAdapter(getContext(), topAlbumsList);
                topAlbumsRecyclerView.setAdapter(topAlbumRecyclerAdapter);
                topAlbumRecyclerAdapter.setOnTopAlbumListener((album_title, artist_name) -> albumListener.onAlbumClick(album_title, artist_name));
            }

            @Override
            public void onError(LfmError error) {

                Log.e("error", error.errorMessage);
            }
        });
    }

    public interface AlbumListener {

        void onAlbumClick(String album_title, String artist_name);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_albums, container, false);
        initializeViews(view);
        getTopAlbums();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {

            albumListener = (AlbumListener) context;

        } catch (Exception e) {

            throw new ClassCastException(context.toString() + " must implement AlbumListener!");

        }

    }

    @Override
    public void onDetach() {
        super.onDetach();

        albumListener = null;
    }
}
