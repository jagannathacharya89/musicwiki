package com.music.musicwiki.view.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;
import com.music.musicwiki.R;
import com.music.musicwiki.model.Genres;

import java.util.List;

public class GenreRecyclerAdapter extends RecyclerView.Adapter<GenreRecyclerAdapter.GenreViewHolder> {

    private List<Genres> genresList;
    private OnGenreClickListener onGenreClickListener;

    public GenreRecyclerAdapter(List<Genres> genresList) { this.genresList = genresList; }

    public interface OnGenreClickListener { void onGenreClick(String genreTitle);}

    public void setOnGenreClickListener(OnGenreClickListener onGenreClickListener) {

        this.onGenreClickListener = onGenreClickListener;
    }

    public void updateTheList(List<Genres> newGenreList) {

        if (!genresList.isEmpty()) { genresList = newGenreList; }

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public GenreViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_genre_item, parent, false);
        return new GenreViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GenreViewHolder holder, int position) {

        holder.genreTitleTextView.setText(genresList.get(position).getGenre());
        holder.genreCardView.setOnClickListener(v -> onGenreClickListener.onGenreClick(genresList.get(position).getGenre()));
    }

    @Override
    public int getItemCount() { return genresList.size(); }

    public static class GenreViewHolder extends RecyclerView.ViewHolder {

        MaterialCardView genreCardView;
        TextView genreTitleTextView;

        public GenreViewHolder(@NonNull View itemView) {
            super(itemView);

            genreCardView = itemView.findViewById(R.id.genreCardView);
            genreTitleTextView = itemView.findViewById(R.id.genreTitleTextView);
        }
    }
}
