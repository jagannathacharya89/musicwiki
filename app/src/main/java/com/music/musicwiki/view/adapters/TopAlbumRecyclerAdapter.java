package com.music.musicwiki.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.music.musicwiki.R;
import com.music.musicwiki.model.TopAlbums;

import java.util.List;

public class TopAlbumRecyclerAdapter extends RecyclerView.Adapter<TopAlbumRecyclerAdapter.TopAlbumViewHolder> {

    private final Context context;
    private final List<TopAlbums> topAlbumsList;
    private TopAlbumListener topAlbumListener;

    public TopAlbumRecyclerAdapter(Context context, List<TopAlbums> topAlbumsList) {

        this.context = context;
        this.topAlbumsList = topAlbumsList;
    }

    public interface TopAlbumListener {

        void onTopAlbumClick(String album_title, String artist_name);
    }

    public void setOnTopAlbumListener(TopAlbumListener topAlbumListener) {

        this.topAlbumListener = topAlbumListener;
    }

    @NonNull
    @Override
    public TopAlbumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_top_albums_artists_tracks, parent, false);
        return new TopAlbumViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TopAlbumViewHolder holder, int position) {

        //Glide.with(context).load(topAlbumsList.get(position).getAlbum_cover()).into(holder.albumCoverImageVIew);
        holder.albumCoverImageVIew.setImageResource(R.drawable.music);
        holder.albumTitleTextView.setText(topAlbumsList.get(position).getAlbum_title());
        holder.artistNameTextView.setText(topAlbumsList.get(position).getArtist_name());
        holder.albumCardView.setOnClickListener(v -> topAlbumListener.onTopAlbumClick(topAlbumsList.get(position).getAlbum_title(), topAlbumsList.get(position).getArtist_name()));
    }

    @Override
    public int getItemCount() {

        return topAlbumsList.size();
    }

    public static class TopAlbumViewHolder extends RecyclerView.ViewHolder {

        CardView albumCardView;
        ImageView albumCoverImageVIew;
        TextView albumTitleTextView, artistNameTextView;

        public TopAlbumViewHolder(@NonNull View itemView) {
            super(itemView);

            albumCardView = itemView.findViewById(R.id.albumCardView);
            albumCoverImageVIew = itemView.findViewById(R.id.albumCoverImageVIew);
            albumTitleTextView = itemView.findViewById(R.id.albumTitleTextView);
            artistNameTextView = itemView.findViewById(R.id.artistNameTextView);
        }
    }
}
