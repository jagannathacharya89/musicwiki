package com.music.musicwiki.view.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.ag.lfm.LfmError;
import com.ag.lfm.LfmParameters;
import com.ag.lfm.LfmRequest;
import com.ag.lfm.api.methods.LfmApiTag;
import com.google.android.material.card.MaterialCardView;
import com.music.musicwiki.R;
import com.music.musicwiki.view.adapters.GenreRecyclerAdapter;
import com.music.musicwiki.model.Genres;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    private boolean isDropDown = false;
    private CircleImageView dropdown_menu_icon;
    private List<Genres> genresList, modifiedGenresList;
    private MaterialCardView dropdown_menu_cv;
    private GenreRecyclerAdapter genreRecyclerAdapter;
    private RecyclerView genreRecyclerView;

    public static String GENRE_TITLE = "Genre_Title";

    private void initializeViews() {

        dropdown_menu_cv = findViewById(R.id.dropdown_menu_cv);
        dropdown_menu_icon = findViewById(R.id.dropdown_menu_icon);
        genresList = new ArrayList<>();
        modifiedGenresList = new ArrayList<>();
        genreRecyclerView = findViewById(R.id.genreRecyclerView);
        genreRecyclerView.setHasFixedSize(true);
        genreRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initializeViews();
        LfmApiTag lfmApiTag = new LfmApiTag();
        LfmParameters params = new LfmParameters();
        lfmApiTag.getTopTags(params).executeWithListener(new LfmRequest.LfmRequestListener() {
            @Override
            public void onComplete(JSONObject response) {

                genresList.clear();
                JSONArray topTags = Objects.requireNonNull(response.optJSONObject("toptags")).optJSONArray("tag");

                for (int i = 0; i < Objects.requireNonNull(topTags).length(); i++) {

                    genresList.add(new Genres(topTags.optJSONObject(i).optString("name")));

                }

                modifiedGenresList = genresList.subList(0, 10);
                genreRecyclerAdapter = new GenreRecyclerAdapter(modifiedGenresList);
                genreRecyclerView.setAdapter(genreRecyclerAdapter);
                genreRecyclerAdapter.setOnGenreClickListener(genreTitle -> {

                    Intent intent = new Intent(MainActivity.this, GenreDetailsActivity.class);
                    intent.putExtra(GENRE_TITLE, genreTitle);
                    startActivity(intent);
                });
            }

            @Override
            public void onError(LfmError error) {

                Log.e("error", error.errorMessage);
            }
        });

        dropdown_menu_cv.setOnClickListener(v -> {

            if (!isDropDown) {

                dropdown_menu_icon.setImageResource(R.drawable.drop_down_menu_arrow_up);
                genreRecyclerAdapter.updateTheList(genresList);
                isDropDown = true;

            } else {

                if (genresList.size() > 10) {

                    dropdown_menu_icon.setImageResource(R.drawable.drop_down_menu_arrow_down);
                    genreRecyclerAdapter.updateTheList(modifiedGenresList);
                    isDropDown = false;

                }

            }

        });
    }
}