package com.music.musicwiki.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.music.musicwiki.R;
import com.music.musicwiki.model.TopArtists;

import java.util.List;

public class TopArtistsRecyclerAdapter extends RecyclerView.Adapter<TopArtistsRecyclerAdapter.TopArtistsViewHolder> {

    private final Context context;
    private final List<TopArtists> topArtistsList;
    private TopArtistListener topArtistListener;

    public TopArtistsRecyclerAdapter(Context context, List<TopArtists> topArtistsList) {

        this.context = context;
        this.topArtistsList = topArtistsList;
    }

    public interface TopArtistListener {

        void onTopArtistClick(String artist_name);
    }

    public void setOnTopAlbumListener(TopArtistListener topArtistListener) {

        this.topArtistListener = topArtistListener;
    }

    @NonNull
    @Override
    public TopArtistsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_top_albums_artists_tracks, parent, false);
        return new TopArtistsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TopArtistsViewHolder holder, int position) {

        holder.artistImageVIew.setImageResource(R.drawable.music);
        holder.artistNameTextView.setText(topArtistsList.get(position).getArtist_name());
        holder.artistCardView.setOnClickListener(v -> topArtistListener.onTopArtistClick(topArtistsList.get(position).getArtist_name()));
    }

    @Override
    public int getItemCount() {

        return topArtistsList.size();
    }

    public static class TopArtistsViewHolder extends RecyclerView.ViewHolder {

        CardView artistCardView;
        ImageView artistImageVIew;
        TextView artistNameTextView;

        public TopArtistsViewHolder(@NonNull View itemView) {
            super(itemView);

            artistCardView = itemView.findViewById(R.id.albumCardView);
            artistImageVIew = itemView.findViewById(R.id.albumCoverImageVIew);
            artistNameTextView = itemView.findViewById(R.id.artistNameTextView);
        }
    }
}
