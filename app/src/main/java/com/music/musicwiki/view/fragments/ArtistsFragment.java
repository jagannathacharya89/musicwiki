package com.music.musicwiki.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ag.lfm.LfmError;
import com.ag.lfm.LfmParameters;
import com.ag.lfm.LfmRequest;
import com.ag.lfm.api.methods.LfmApiTag;
import com.music.musicwiki.R;
import com.music.musicwiki.view.adapters.TopArtistsRecyclerAdapter;
import com.music.musicwiki.model.TopArtists;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ArtistsFragment extends Fragment {

    private ArtistListener artistListener;
    private final String api_key, tag;
    private LfmApiTag lfmApiTag;
    private LfmParameters params;
    private List<TopArtists> topArtistsList;
    private RecyclerView topArtistsRecyclerView;

    public ArtistsFragment(String api_key, String tag) {

        this.api_key = api_key;
        this.tag = tag;
    }

    private void getTopArtists() {

        params.put("tag", tag);
        params.put("api_key", api_key);
        lfmApiTag.getTopArtists(params).executeWithListener(new LfmRequest.LfmRequestListener() {
            @Override
            public void onComplete(JSONObject response) {

                topArtistsList.clear();
                JSONArray topAlbums = Objects.requireNonNull(response.optJSONObject("topartists")).optJSONArray("artist");

                for (int i = 0; i < Objects.requireNonNull(topAlbums).length(); i++) {

                    String artist_name = topAlbums.optJSONObject(i).optString("name");
                    String artist_image = topAlbums.optJSONObject(i).optString("url");
                    Log.d("artist_image", artist_image);
                    topArtistsList.add(new TopArtists(
                            artist_image,
                            artist_name)
                    );

                }

                TopArtistsRecyclerAdapter topArtistsRecyclerAdapter = new TopArtistsRecyclerAdapter(getContext(), topArtistsList);
                topArtistsRecyclerView.setAdapter(topArtistsRecyclerAdapter);
                topArtistsRecyclerAdapter.setOnTopAlbumListener(artist_name -> artistListener.onArtistClick(artist_name));
            }

            @Override
            public void onError(LfmError error) {

                Log.e("error", error.errorMessage);
            }
        });
    }

    private void initializeViews(View view) {

        lfmApiTag = new LfmApiTag();
        params = new LfmParameters();
        topArtistsList = new ArrayList<>();
        topArtistsRecyclerView = view.findViewById(R.id.topArtistsRecyclerView);
        topArtistsRecyclerView.setHasFixedSize(true);
        topArtistsRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
    }

    public interface ArtistListener {

        void onArtistClick(String artist_name);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_artists, container, false);
        initializeViews(view);
        getTopArtists();
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {

            artistListener = (ArtistListener) context;

        } catch (Exception e) {

            throw new ClassCastException(context.toString() + " must implement ArtistListener!");

        }

    }

    @Override
    public void onDetach() {
        super.onDetach();

        artistListener = null;
    }
}
