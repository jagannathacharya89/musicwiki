package com.music.musicwiki.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.music.musicwiki.R;
import com.music.musicwiki.model.TopTracks;

import java.util.List;

public class TopTracksRecyclerAdapter extends RecyclerView.Adapter<TopTracksRecyclerAdapter.TopTracksViewHolder> {

    private final Context context;
    private final List<TopTracks> topTracksList;

    public TopTracksRecyclerAdapter(Context context, List<TopTracks> topTracksList) {

        this.context = context;
        this.topTracksList = topTracksList;
    }

    @NonNull
    @Override
    public TopTracksViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_top_albums_artists_tracks, parent, false);
        return new TopTracksViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TopTracksViewHolder holder, int position) {

        holder.trackCoverImageVIew.setImageResource(R.drawable.music);
        holder.trackTitleTextView.setText(topTracksList.get(position).getTrack_title());
        holder.artistNameTextView.setText(topTracksList.get(position).getArtist_name());
    }

    @Override
    public int getItemCount() {

        return topTracksList.size();
    }

    public static class TopTracksViewHolder extends RecyclerView.ViewHolder {

        CardView trackCardView;
        ImageView trackCoverImageVIew;
        TextView trackTitleTextView, artistNameTextView;

        public TopTracksViewHolder(@NonNull View itemView) {
            super(itemView);

            trackCardView = itemView.findViewById(R.id.albumCardView);
            trackCoverImageVIew = itemView.findViewById(R.id.albumCoverImageVIew);
            trackTitleTextView = itemView.findViewById(R.id.albumTitleTextView);
            artistNameTextView = itemView.findViewById(R.id.artistNameTextView);
        }
    }
}
