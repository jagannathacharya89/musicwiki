package com.music.musicwiki.view.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ag.lfm.LfmError;
import com.ag.lfm.LfmParameters;
import com.ag.lfm.LfmRequest;
import com.ag.lfm.api.methods.LfmApiTag;
import com.music.musicwiki.R;
import com.music.musicwiki.view.adapters.TopTracksRecyclerAdapter;
import com.music.musicwiki.model.TopTracks;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TracksFragment extends Fragment {

    private final String api_key, tag;
    private LfmApiTag lfmApiTag;
    private LfmParameters params;
    private List<TopTracks> topTracksList;
    private RecyclerView topTracksRecyclerView;

    public TracksFragment(String api_key, String tag) {

        this.api_key = api_key;
        this.tag = tag;
    }

    private void getTopTracks() {

        params.put("tag", tag);
        params.put("api_key", api_key);
        lfmApiTag.getTopTracks(params).executeWithListener(new LfmRequest.LfmRequestListener() {
            @Override
            public void onComplete(JSONObject response) {

                topTracksList.clear();
                JSONArray topTracks = Objects.requireNonNull(response.optJSONObject("tracks")).optJSONArray("track");

                for (int i = 0; i < Objects.requireNonNull(topTracks).length(); i++) {

                    String artist_name = Objects.requireNonNull(topTracks.optJSONObject(i).optJSONObject("artist")).optString("name");
                    String track_name = topTracks.optJSONObject(i).optString("name");
                    String track_cover = topTracks.optJSONObject(i).optString("url");
                    topTracksList.add(new TopTracks(
                            track_cover,
                            track_name,
                            artist_name)
                    );

                }

                TopTracksRecyclerAdapter topTracksRecyclerAdapter = new TopTracksRecyclerAdapter(getContext(), topTracksList);
                topTracksRecyclerView.setAdapter(topTracksRecyclerAdapter);
            }

            @Override
            public void onError(LfmError error) {

                Log.e("error", error.errorMessage);
            }
        });
        /*
        lfmApiTag.getTopArtists(params).executeWithListener(new LfmRequest.LfmRequestListener() {
            @Override
            public void onComplete(JSONObject response) {

                topArtistsList.clear();
                Log.d("top artists", response.toString());
                JSONArray topAlbums = response.optJSONObject("topartists").optJSONArray("artist");

                for (int i = 0; i < topAlbums.length(); i++) {

                    String artist_name = topAlbums.optJSONObject(i).optString("name");
                    String artist_image = topAlbums.optJSONObject(i).optString("url");
                    Log.d("artist_image", artist_image);
                    topArtistsList.add(new TopArtists(
                            artist_image,
                            artist_name)
                    );

                }

                TopArtistsRecyclerAdapter topArtistsRecyclerAdapter = new TopArtistsRecyclerAdapter(getContext(), topArtistsList);
                topArtistsRecyclerView.setAdapter(topArtistsRecyclerAdapter);
            }

            @Override
            public void onError(LfmError error) {


            }
        });

         */
    }

    private void initializeViews(View view) {

        lfmApiTag = new LfmApiTag();
        params = new LfmParameters();
        topTracksList = new ArrayList<>();
        topTracksRecyclerView = view.findViewById(R.id.topTracksRecyclerView);
        topTracksRecyclerView.setHasFixedSize(true);
        topTracksRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_tracks, container, false);
        initializeViews(view);
        getTopTracks();
        return view;
    }
}
