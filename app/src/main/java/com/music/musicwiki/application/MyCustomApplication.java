package com.music.musicwiki.application;

import android.app.Application;

import com.ag.lfm.Lfm;

public class MyCustomApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Lfm.initializeWithSecret(this);
    }
}
