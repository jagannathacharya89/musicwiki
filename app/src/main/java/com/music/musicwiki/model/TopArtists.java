package com.music.musicwiki.model;

public class TopArtists {

    String artist_image, artist_name;

    public TopArtists(String artist_image, String artist_name) {

        this.artist_image = artist_image;
        this.artist_name = artist_name;
    }

    public String getArtist_image() {
        return artist_image;
    }

    public String getArtist_name() {
        return artist_name;
    }
}
