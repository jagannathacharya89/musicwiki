package com.music.musicwiki.model;

public class Genres {

    private final String genre;

    public Genres(String genre) { this.genre = genre; }

    public String getGenre() { return genre; }
}
