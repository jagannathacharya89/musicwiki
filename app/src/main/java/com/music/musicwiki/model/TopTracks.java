package com.music.musicwiki.model;

public class TopTracks {

    String track_cover, track_title, artist_name;

    public TopTracks(String track_cover, String track_title, String artist_name) {

        this.artist_name = artist_name;
        this.track_cover = track_cover;
        this.track_title = track_title;
    }

    public String getTrack_cover() {
        return track_cover;
    }

    public String getTrack_title() {
        return track_title;
    }

    public String getArtist_name() {
        return artist_name;
    }
}
