package com.music.musicwiki.model;

public class TopAlbums {

    String album_cover, album_title, artist_name;

    public TopAlbums(String album_cover, String album_title, String artist_name) {

        this.album_cover = album_cover;
        this.album_title = album_title;
        this.artist_name = artist_name;
    }

    public String getAlbum_cover() {
        return album_cover;
    }

    public String getAlbum_title() {
        return album_title;
    }

    public String getArtist_name() {
        return artist_name;
    }
}
